import cv2
#doc anh
img = cv2.imread('maume.jpg')
#hien thi anh
cv2.imshow("maume", img)
# Tạo đối tượng VideoCapture()
cap = cv2.VideoCapture(0)

# # Vòng lặp đọc ảnh từ camera và hiển thị ảnh
# while True:
#     # Đọc ảnh từ camera
#     ret, frame = cap.read()

#     # Hiển thị ảnh từ camera
#     cv2.imshow("Ảnh", frame) #(Ảnh là tên cửa sổ hiển thị ảnh)

#     # Kiểm tra nếu người dùng nhấn phím Esc để thoát
#     if cv2.waitKey(1) == 27:
#         break

# # Đóng camera
# cap.release()
#Cắt ảnh
cropped_img = img[100:200, 100:200]

#Vẽ hình chữ nhật
x1, y1, x2, y2 = 100, 100, 200, 200
cv2.rectangle(img, (x1, y1), (x2, y2), (255, 0, 0), 2)
# Đặt text
text = "Hello, world!"
org = (100, 100)
fontFace = cv2.FONT_HERSHEY_SIMPLEX
fontScale = 1
color = (255, 0, 0)
thickness = 2
lineType               = 2

bottomLeftOrigin = False
cv2.putText(img, text, org, fontFace, fontScale, color, thickness,lineType, bottomLeftOrigin)
#Làm mờ ảnh
img_blur = cv2.GaussianBlur(img,(15,15),0)
#Chuyển đổi ảnh sang không gian màu HSV
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

# Lọc màu đỏ
lower_red = (0, 245, 240)
upper_red = (10, 255, 255)
red_mask = cv2.inRange(hsv, lower_red, upper_red)

# Lấy kết quả lọc
red_img = cv2.bitwise_and(img, img, mask=red_mask)

# Hiển thị ảnh
cv2.imshow("Red image", red_img)
#cv2.imshow('anh',img_blur)
cv2.waitKey(0)
